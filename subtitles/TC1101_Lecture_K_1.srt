1
00:00:03,640 --> 00:00:09,320
We already covered parameter encryption in our 
lecture G, and how to use it with the tpm2-tools.

2
00:00:09,320 --> 00:00:16,000
It is important to remember that to enable parameter 
encryption we need to create a TPM session. And out  

3
00:00:16,000 --> 00:00:22,040
of the four types of TPM sessions there are 
only three that support parameter encryption. 

4
00:00:22,040 --> 00:00:28,400
In lecture G we focused on HMAC sessions and 
policy sessions and for a good reason. Audit  

5
00:00:28,400 --> 00:00:33,720
sessions have a special purpose. Here I'll give 
a bit of information about audit sessions for  

6
00:00:33,720 --> 00:00:39,520
the curious students. While audit sessions are out 
of scope for this course I want you to have this  

7
00:00:39,520 --> 00:00:45,200
information to understand why audit sessions 
are not the best fit if you just want to use  

8
00:00:45,200 --> 00:00:51,160
parameter encryption. When audit sessions are 
present the TPM is instructed to generate hash  

9
00:00:51,160 --> 00:00:56,840
over the command and its parameters and response 
when executing. The value is stored and later  

10
00:00:56,840 --> 00:01:02,840
can be retrieved with a dedicated TPM2 command. 
Audit sessions support parameter encryption as  

11
00:01:02,840 --> 00:01:08,640
we mentioned. So this computation and execution 
of your TPM2 command is protected while this  

12
00:01:08,640 --> 00:01:14,720
happens. At the same time if you don't need this 
audit capability of creating a log trail for the  

13
00:01:14,720 --> 00:01:19,840
commands and their execution, then using audit 
session to enable parameter encryption is a bit  

14
00:01:19,840 --> 00:01:27,000
of an overhead. It is much better to just use HMAC 
sessions. Going back to what we know so far about  

15
00:01:27,000 --> 00:01:33,960
parameter encryption, this feature enables the TPM 
to protect the very first parameter of a command  

16
00:01:33,960 --> 00:01:38,800
that supports parameter encryption. Why not all 
commands support it? Because not every command  

17
00:01:38,800 --> 00:01:44,560
has sensitive information. In fact in some cases it 
might be only the command itself we are requesting  

18
00:01:44,560 --> 00:01:49,960
from the TPM, or it is only the response that 
contains the sensitive information. For example  

19
00:01:49,960 --> 00:01:55,560
when reading an NV index. Remember the interesting 
case with the symmetric encryption command in  

20
00:01:55,560 --> 00:02:02,520
the TPM? There is now a newer version and the 
original one was deprecated the tpm2_encryptdecrypt2  

21
00:02:02,520 --> 00:02:08,840
makes one significant change compared 
to the previous command: it moves the data payload  

22
00:02:08,840 --> 00:02:13,640
as a first parameter in the common structure. 
This means that now using parameter encryption  

23
00:02:13,640 --> 00:02:19,760
this payload can be protected when sent to the 
TPM. I know this looks too easy, especially when  

24
00:02:19,760 --> 00:02:24,640
we are using the tpm2-tools, which make it so 
straightforward to enable parameter encryption.  

25
00:02:24,640 --> 00:02:31,480
But did you know that in fact we can enable only 
the sending of the data to the TPM to be encrypted?  

26
00:02:31,480 --> 00:02:36,320
Or vice versa only the response from the TPM 
to be encrypted, but our command request to  

27
00:02:36,320 --> 00:02:41,680
be non encrypted. Let's look under the hood what 
happens when we use an API. What was hidden from  

28
00:02:41,680 --> 00:02:49,240
us when we were using the tpm2-tools was the 
different attributes. Session attributes more  

29
00:02:49,240 --> 00:02:56,240
or less define how the session operates. For what 
it can be used. There is a detailed explanation  

30
00:02:56,240 --> 00:03:02,400
in the TPM specification, and I have put here 
the chapter and the reference to that part.

31
00:03:02,400 --> 00:03:09,000
These session attributes are set in one byte field. The 
field name is important because we need it later:  

32
00:03:09,000 --> 00:03:15,240
it is TPMA_SESSION (capital letters). This 
means that every attribute is in fact a bit  

33
00:03:15,240 --> 00:03:21,320
field. Depending on if it's set or clear there is 
different behavior in the TPM. The most important  

34
00:03:21,320 --> 00:03:26,800
session attribute is the continueSession. 
This specifies if after the execution of the  

35
00:03:26,800 --> 00:03:33,160
command the TPM session expires. Out of the available 
attributes these three are the important ones for  

36
00:03:33,160 --> 00:03:40,280
parameter encryption. continueSession enables a 
TPM session to live on after the command execution.  

37
00:03:40,280 --> 00:03:46,040
Otherwise by default it expires it is not active 
and we need to create a new one. You didn't know  

38
00:03:46,040 --> 00:03:51,880
about this because the tpm2-tools conveniently 
by default set this flag alongside the other two  

39
00:03:51,880 --> 00:03:56,400
that we're going to look in a moment. Now we come 
to the interesting part, that actually parameter  

40
00:03:56,400 --> 00:04:02,480
encryption gives us the choice if you want all the 
communication to be encrypted or only the command request  

41
00:04:02,480 --> 00:04:08,040
that we sent, and the TPM has the responsibility 
to decrypt it before unmarshalling what we sent.  

42
00:04:08,040 --> 00:04:14,640
And vice versa we can have a situation where we 
instruct the TPM that it will receive a plain  

43
00:04:14,640 --> 00:04:19,440
command but the response has to be encrypted. 
And then this is the responsibility of the  

44
00:04:19,440 --> 00:04:26,640
stack to decrypt the payload for us given 
the session information that we provided.

