1
00:00:02,040 --> 00:00:07,800
Welcome to this cyber security course. 
Here we will gain practical skills how  

2
00:00:07,800 --> 00:00:15,800
to use hardware security to protect better our 
systems. And we will gain the fundamental  

3
00:00:15,800 --> 00:00:23,000
knowledge about trusted computing. My name 
is Dimi and I'm the founder of the largest  

4
00:00:23,000 --> 00:00:29,480
developers community for hardware security. My 
background is industrial automation, however  

5
00:00:29,480 --> 00:00:37,120
for the past 10 years I have worked mainly on 
Internet of Things projects. Feel free to reach  

6
00:00:37,120 --> 00:00:44,200
out to me on my email or better yet leave 
me a message in the discussion forums in

7
00:00:44,200 --> 00:00:53,000
OST2. The motivation for this course is to have 
better security protection for our systems and our  

8
00:00:53,000 --> 00:01:01,920
data. Large companies are already using hardware 
security on a daily basis. For example, Microsoft  

9
00:01:01,920 --> 00:01:12,480
uses trusted platform modules to protect their 
end users. Amazon provides virtual trusted platform  

10
00:01:12,480 --> 00:01:23,560
modules to their cloud customers and users. Apple 
also with a custom silicon solution protects its  

11
00:01:23,560 --> 00:01:30,040
end users. Thanks to hardware security we 
have higher guarantees than only software  

12
00:01:30,040 --> 00:01:39,360
based security. Simply said, software only 
security is no longer enough. This course will  

13
00:01:39,360 --> 00:01:46,640
help you use widely available tools and hardware 
security modules called trusted platform modules  

14
00:01:46,640 --> 00:01:56,600
or for short TPM. This is the first of just a few 
introductory lectures teaching us about what is  

15
00:01:56,600 --> 00:02:06,040
a TPM and when it makes the most sense to use it, 
when can you receive the highest impact for your  

16
00:02:06,040 --> 00:02:15,560
system security, or your data security, application 
security, and so on. Then, we will go into setting up  

17
00:02:15,560 --> 00:02:22,920
our laboratory environment for the exercises and 
there are many exercises in this course we have  

18
00:02:22,920 --> 00:02:35,520
prepared for you. The purpose is to build practical 
skills using a TPM. Then we will dive deep into the  

19
00:02:35,520 --> 00:02:44,800
core curriculum with many real life use cases 
of a TPM and hardware backed security; protecting  

20
00:02:44,800 --> 00:02:53,840
the file system, or individual files, protecting 
arbitrary data, secrets, user data or system data.  

21
00:02:53,840 --> 00:03:02,800
We will also learn how to use the built-in protection 
of the TPM against Machine-in-the-Middle attacks.  

22
00:03:02,800 --> 00:03:09,960
Because the TPM usually is a physical device and 
there is a bus, a communication, between the host  

23
00:03:09,960 --> 00:03:18,160
and the TPM, even in the cases when the TPM is 
just a software module memory isolated and so  

24
00:03:18,160 --> 00:03:27,960
on. And here is the place to mention what is 
TPM? TPM is a type of hardware security module.  

25
00:03:27,960 --> 00:03:37,720
hardware security modules typically offer strong 
cryptography, high protection of the generated  

26
00:03:37,720 --> 00:03:46,320
cryptographic keys, also capabilities like digital 
signing, and others. The TPM is special in that way  

27
00:03:46,320 --> 00:03:54,640
because it offers all of that and more. Including 
non-volatile memory that can benefit from the TPM  

28
00:03:54,640 --> 00:04:03,480
2.0 authorization that is used to protect how and 
when do we use keys, who can use our keys, for what  

29
00:04:03,480 --> 00:04:11,800
operations. The TPM builds on top of that adding 
attestation capabilities, special PCR register that  

30
00:04:11,800 --> 00:04:21,960
we'll cover in another course. Here, we will focus 
on cryptography, digital signing, secrets, using  

31
00:04:21,960 --> 00:04:30,280
the NVRAM secure storage, and more. A few words 
about our laboratory environment that will use  

32
00:04:30,280 --> 00:04:35,960
to run the many exercises that we have prepared 
for you. Typically the TPM is a dedicated device  

33
00:04:35,960 --> 00:04:41,920
or a dedicated software module that could 
come in a form of a firmware or part of the  

34
00:04:41,920 --> 00:04:47,640
UEFI, there are many variants out there. If you are 
using a work laptop it is very possible that the  

35
00:04:47,640 --> 00:04:54,440
TPM is already being used by your organization to 
manage and protect the work computer that you that  

36
00:04:54,440 --> 00:05:00,760
you're having. At the same time, if you're running 
a Microsoft Windows or Linux it could by default  

37
00:05:00,760 --> 00:05:07,240
enable some protection that leverages the TPM 
to provide you higher security already. To avoid  

38
00:05:07,240 --> 00:05:14,560
breaking your system, we are offering a ready to 
use Docker environment. Also there is a way to run  

39
00:05:14,560 --> 00:05:22,040
all the exercises on the web using free of charge 
service provided by Docker. That being said you can  

40
00:05:22,040 --> 00:05:29,880
also run local. Of course this has its risks. We 
will provide instructions for all of these three  

41
00:05:29,880 --> 00:05:37,560
scenarios. And we recommend running locally with 
our Docker environment. This way you can quickly  

42
00:05:37,560 --> 00:05:44,080
get into the exercises and also try out different 
things without worrying about breaking your system . 

43
00:05:44,080 --> 00:05:49,920
We're using open-source software stacks and we'll 
cover these in a moment. This course will offer  

44
00:05:49,920 --> 00:05:57,720
you both an approach that uses ready-to-use tools, 
and a separate one where you get behind the scenes  

45
00:05:57,720 --> 00:06:05,320
look of what it is to build a solution using the 
API. However, this is just a snapshot and some highlights  

46
00:06:05,320 --> 00:06:12,320
of the exercise that we have prepared for you. 
All of the exercises here are inspired by real  

47
00:06:12,320 --> 00:06:19,720
life situations and real life experience. There 
is a great benefit in making (doing) all of the exercises.  

48
00:06:19,720 --> 00:06:26,640
Also we've tried to make the learning curve as 
gradual as possible so we recommend making (doing) the  

49
00:06:26,640 --> 00:06:33,640
exercises in order. To talk to the TPM we need to 
learn the TPM commands. But this would take us a lot  

50
00:06:33,640 --> 00:06:41,880
of time. Instead we will use a TPM software stack. 
This is just a software library that provides us  

51
00:06:41,880 --> 00:06:50,680
rich API to perform the various TPM operations 
with this. There are at least five well-known TPM  

52
00:06:50,680 --> 00:06:57,000
stacks with open-source license out there. Each 
of them has a strong side, so it really depends  

53
00:06:57,000 --> 00:07:03,960
on your environment and your requirements. 
For example the tpm2-TSS stack conform to the  

54
00:07:03,960 --> 00:07:11,000
TCG standard this is the only stack that provides 
the three standard APIs. We have SAPI, the system  

55
00:07:11,000 --> 00:07:19,280
API, ESAPI, the enhanced API, and we have FAPI, 
the feature API. The latter is the most developer  

56
00:07:19,280 --> 00:07:24,880
friendly. This is great if you're a beginner 
because it has a separate project called  

57
00:07:24,880 --> 00:07:30,760
tpm2-tools, what we'll be using in this course at the 
beginning, and it helps you to quickly start using  

58
00:07:30,760 --> 00:07:38,640
the TPM. And a lot of the tools match the names 
of the TPM to commands. On the other hand if you  

59
00:07:38,640 --> 00:07:48,840
have a Windows environment and you would like to 
use a .NET API, Microsoft have the MSR stack, the TPM  

60
00:07:48,840 --> 00:07:55,240
stack, that comes with a C# API. We also have the 
IBM stack and the Google stack. Of course these  

61
00:07:55,240 --> 00:08:00,640
projects are open-source so I'm just naming the 
companies that started the project. Nowadays there  

62
00:08:00,640 --> 00:08:07,160
are many companies and individual contributors to 
these stacks to these libraries. The go-tpm stack, as  

63
00:08:07,160 --> 00:08:14,160
the names suggests, provides go-lang API, which is great 
for server and Cloud applications. On the other  

64
00:08:14,160 --> 00:08:20,120
hand the wolfTPM stack is designed for embedded 
systems. So it is really up to the developer to  

65
00:08:20,120 --> 00:08:27,840
decide which TPM software stack suits you best. In 
this course we will start with the tpm2 TSS stack  

66
00:08:27,840 --> 00:08:34,480
and the tpm2-tools. And we will move gradually to 
also exploring behind the scenes with some of the  

67
00:08:34,480 --> 00:08:42,680
APIs. We already mentioned what is a TPM and HSM. Now comes the time to talk about a hardware root of  

68
00:08:42,680 --> 00:08:49,720
trust. What is a root of trust? This is something 
we can always trust thanks to a cryptographic  

69
00:08:49,720 --> 00:08:56,520
strength, thanks to its cryptographic system. This 
is where the trust in our computer system begins.  

70
00:08:56,520 --> 00:09:02,240
This is why it's so important. The TPM is is one 
way to achieve this. The great thing about the  

71
00:09:02,240 --> 00:09:07,600
TPM is that you can connect it externally to the 
system or you can have it internally as a firmware  

72
00:09:07,600 --> 00:09:14,080
or software module. To understand why root of trust 
is important and what kind of benefits it gives us  

73
00:09:14,080 --> 00:09:20,640
I would like to make the analogy a comparison how 
we travel with airplanes. We have an ID document  

74
00:09:20,640 --> 00:09:27,560
that is a proof of who we are. Based on that 
proof, an identity check, we're either allowed  

75
00:09:27,560 --> 00:09:33,400
inside the airport to reach the gate and board the 
plane or we're turned back. Depending on where you live  

76
00:09:33,400 --> 00:09:39,640
the ID card is usually issued by a government 
entity. Here in Bulgaria this is the ministry of  

77
00:09:39,640 --> 00:09:46,600
interior. And when at first the police officers or 
the customs agent looks at you they check the  

78
00:09:46,600 --> 00:09:52,200
physical traits that you have and compare it with 
the information that is on the ID card. What is the  

79
00:09:52,200 --> 00:09:57,360
color of your hair, what's the color of your eyes, 
maybe the height, the name, do you respond to your  

80
00:09:57,360 --> 00:10:03,440
name, these kind of things. This is just the 
first step. The next step is the police officer to  

81
00:10:03,440 --> 00:10:10,120
actually perform a validity check of your ID 
by asking the authority that created, crafted,  

82
00:10:10,120 --> 00:10:17,320
issued the ID to verify that this is a genuine ID. 
Now this is similar to what a TPM can do for our  

83
00:10:17,320 --> 00:10:24,640
computer system. A TPM can provide us this root of 
trust by performing secure digital signatures. Also  

84
00:10:24,640 --> 00:10:31,080
to create highly secure reports about the claims 
that we made make the state of the system and so on. 

85
00:10:31,080 --> 00:10:36,840
This is possible thanks to special platform 
configuration registers called PCRs that we'll  

86
00:10:36,840 --> 00:10:42,760
learn in the advanced course. For now let's 
just stick with the identity. The TPM provides  

87
00:10:42,760 --> 00:10:49,640
a unique private key material that never leaves 
the TPM. It can be created and recreated again and  

88
00:10:49,640 --> 00:10:55,000
again inside the TPM and it can serve as a machine 
identity. There're pros to doing that, and there are  

89
00:10:55,000 --> 00:11:01,600
some disadvantages. So if you want you can generate 
a different primary key on the TPM to serve as an  

90
00:11:01,600 --> 00:11:08,680
identity. In all these cases the TPM is the only 
entity that can use this key. It lives only inside  

91
00:11:08,680 --> 00:11:14,920
the TPM where it is in a state to be used, create 
digital signatures, authenticate a message, confirm  

92
00:11:14,920 --> 00:11:19,920
the validity of proof that has been generated. 
So this is similar to boarding a plane and  

93
00:11:19,920 --> 00:11:26,320
going through customs where your ID is checked. 
In essence a TPM can help you provide machine  

94
00:11:26,320 --> 00:11:33,040
identity, can help you create health checks, and 
can also securely encrypt and decrypt different  

95
00:11:33,040 --> 00:11:40,760
parts of your system, seal secrets, and so on so on. 
It all starts with the root of trust where we know  

96
00:11:40,760 --> 00:11:48,840
that the TPM provides a high guarantee system with 
cryptographic capabilities. We'll also talk about  

97
00:11:48,840 --> 00:11:54,520
how can we be sure that the TPM is genuine. This 
will come later in the first lecture of our core  

98
00:11:54,520 --> 00:12:01,080
curriculum. Many of you are probably already asking 
if your device that you're using right now, a phone,  

99
00:12:01,080 --> 00:12:09,280
or a laptop, or a desktop computer, has a root of 
trust. Before TPMs were natural to servers and  

100
00:12:09,280 --> 00:12:14,920
desktop computers. Nowadays we could say they're 
almost everywhere. Including there is a trend of  

101
00:12:14,920 --> 00:12:20,840
adding TPMs to embedded systems. But TPM is not 
the only type of hardware security module you  

102
00:12:20,840 --> 00:12:27,480
can use. There are secure element, there are smart 
cards, and so on and so on. Smartphones nowadays  

103
00:12:27,480 --> 00:12:34,960
have a type of HSM depending on the model and the 
manufacturer. So yes, your system most likely 99%  

104
00:12:34,960 --> 00:12:41,800
I would say, has a root of trust. And to use that 
is important. To either protect an application, its  

105
00:12:41,800 --> 00:12:48,080
configuration, or the user data. And this is the 
purpose of this course. To enable you to use the  

106
00:12:48,080 --> 00:12:54,960
TPM for your application for your project where 
whenever it is available. Of course there are  

107
00:12:54,960 --> 00:13:00,040
various implementations as we mentioned earlier 
it could be a physical one it could be a virtual  

108
00:13:00,040 --> 00:13:04,840
one if it's a cloud environment it could be a 
firmware or a software module. What you need to  

109
00:13:04,840 --> 00:13:11,840
know is that the TPM may come in many forms but 
it uses a standardized API. And the interface to  

110
00:13:11,840 --> 00:13:17,440
the TPM is always the same. This means that your 
application would have portability which I think  

111
00:13:17,440 --> 00:13:23,160
is great. So what can we do with a TPM? We can 
protect individual parts of the system and files.  

112
00:13:23,160 --> 00:13:32,040
Also we can use the TPM to manage our private keys 
that are used for remote login, VPN, SSH, and other  

113
00:13:32,040 --> 00:13:40,160
use cases. Including there is a PKCS11 plugin that 
we can use. We can also protect arbitrary data or  

114
00:13:40,160 --> 00:13:46,480
store critical or sensitive information. In some 
cases this could be a certificate, in some cases  

115
00:13:46,480 --> 00:13:52,400
could be a private key, private key material, and 
so on and so on. I've seen many applications for  

116
00:13:52,400 --> 00:14:00,640
example I've seen a counter application which uses 
the TPM as NVRAM to measure the distance traveled  

117
00:14:00,640 --> 00:14:08,040
by a vehicle. I've seen applications where the 
TPM is used to generate proof about the bandwidth  

118
00:14:08,040 --> 00:14:14,640
counter or on a network adapter. And this brings 
us to the next topic. Other than a secure storage,  

119
00:14:14,640 --> 00:14:19,360
thanks to the root of trust for storage that 
the TPM provides, we also have a root of trust  

120
00:14:19,360 --> 00:14:24,519
for reporting. Root of trust for
reporting provides us different ways to

121
00:14:24,519 --> 00:14:29,519
provide evidence about what we claim.
This could be a message, this could be a

122
00:14:29,519 --> 00:14:33,440
hash, this could be the state of the
system or a different metric. As I

123
00:14:33,440 --> 00:14:39,000
mentioned it could be the bandwidth of a
network card; how much traffic has flown

124
00:14:39,000 --> 00:14:43,920
for this month, or this year, and so on.
Including, the TPM provides internal

125
00:14:43,920 --> 00:14:49,000
flags that are available to check if the
TPM has been compromised, has detected an

126
00:14:49,000 --> 00:14:55,399
attempt, or dictionary attack, or some
other form of compromise. And this is

127
00:14:55,399 --> 00:14:59,600
also a great quality of the TPMs.

128
00:14:59,600 --> 00:15:07,200
Today we learned that software-based 
security is no longer enough and we can easily  

129
00:15:07,200 --> 00:15:17,200
upgrade by adding TPM-backed security. To achieve 
this there are TPM software stack libraries that  

130
00:15:17,200 --> 00:15:24,240
we can use depending on our environment. Also there 
are tpm2-tools openly available that can speed up  

131
00:15:24,240 --> 00:15:30,560
our development and getting to know the TPM. And 
this is with what we'll start. One important factor  

132
00:15:30,560 --> 00:15:39,120
of recommending the TPM as a solution is that 
it can have multiple users if needed, multiple  

133
00:15:39,120 --> 00:15:47,080
owners so to speak, thanks to its capability 
to support multiple key hierarchies. And we'll  

134
00:15:47,080 --> 00:15:56,400
talk more about this in the first core 
curriculum that we'll have. Also an important trait  

135
00:15:56,400 --> 00:16:02,760
of the TPM is its built in protection against 
Machine-in-the-Middle. You may have heard about  

136
00:16:02,760 --> 00:16:12,440
TPM sniffing attacks. Usually, in most of the cases, 
it is about misconfiguration. Either by the OEM or  

137
00:16:12,440 --> 00:16:21,080
by an OS vendor. The built-in protection of the TPM 
has to be enabled in order to work. And either it's  

138
00:16:21,080 --> 00:16:30,680
enabled late in the power cycle or it's not 
enabled at all. And because this is recognized  

139
00:16:30,680 --> 00:16:38,880
as a pain point, that in some cases OEM vendors do not 
enable this protection, there is an improvement  

140
00:16:38,880 --> 00:16:47,120
coming that the communication of the TPM will be 
encrypted by default without having to rely on the  

141
00:16:47,120 --> 00:16:53,640
OEM or the vendor to enable that encryption. But 
even today the TPM is secure from Machine-in-the-Middle  

142
00:16:53,640 --> 00:17:01,640
attacks, it's just a matter of knowing 
how to enable that protection, and we take a  

143
00:17:01,640 --> 00:17:09,000
significant focus on this topic in this course. 
Please feel free to send us question questions in  

144
00:17:09,000 --> 00:17:14,800
the discussion box after each lecture this helps 
us understand where the question originated of  

145
00:17:14,800 --> 00:17:19,960
course you can always reach out at our course 
email that's shown on your screen right now

